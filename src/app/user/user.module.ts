import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../shared/material/material.module';

import { UserRoutingModule } from './user-routing.module';
import { User1000Component } from './list/user1000.component';
import { UserComponent } from './user.component';
import { User1010TableComponent } from './list/user1010-table.component';


@NgModule({
  declarations: [User1000Component, UserComponent, User1010TableComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    MaterialModule
  ]
})
export class UserModule { }
