import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/service/user.service';
import { User } from 'src/app/shared/model/user';

@Component({
  selector: 'app-user1010-table',
  templateUrl: './user1010-table.component.html',
  styleUrls: ['./user1010-table.component.scss']
})
export class User1010TableComponent implements OnInit {

    public total: number;
    public displayedColumns: string[] = ['first_name', 'last_name', 'email'];
    public userList: User[] = [];

    // MatPaginator
    public pageSize = 6;
    public pageSizeOptions: number[] = [3, 6];

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
      this.listUsers(1,6);
  }

    listUsers(pageNo: number, pageSize: number) {
        this.userService
            .listUsers(pageNo, pageSize)
            .subscribe((result: {page: number; data: User[]; total: number; }) => {
                this.userList = result.data;
                this.total = result.total;
                console.log(this.userList);
            });
    }   

    onPageSelect(e: any) {
        if (this.userList.length > 0) {
            this.listUsers(e.pageIndex + 1, e.pageSize);
        }
    }

}
