export class User {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    avatar: string;

    // tslint:disable-next-line:ban-types
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }

    fullName() {
        return `${this.last_name}, ${this.first_name}`;
    }
}