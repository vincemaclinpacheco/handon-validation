import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { User } from '../model/user';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private httpClient: HttpClient
  ) { }

public listUsers(page: number, per_page: number): Observable<any> {
    const htppParams = new HttpParams()
                        .set('page', page.toString())
                        .set('per_page', per_page.toString());
    return this.httpClient
        .get(`https://reqres.in/api/users`, { params: htppParams })
        .pipe(map((result: 
                    {   page: number; 
                        per_page: number;
                        total: number;
                        total_pages: number;
                        data: User[]
                    }) => {
            return {
                page  : result.page,
                data  : result.data,
                total : result.total
            };
        }), catchError(this.handleError));
}

private handleError(error: any) {
    return throwError(error);
}
}
